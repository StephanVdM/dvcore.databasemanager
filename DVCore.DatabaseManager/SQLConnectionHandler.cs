﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DVCore.DatabaseManager
{
    public class SQLConnectionHandler
    {
        public static string connectionString;

        private SqlConnection conn;

        public SQLConnectionHandler(string SqlServer, string SqlUsername, string sqlPassword, string edmsDatabase)
        {
            SQLConnectionHandler.connectionString = string.Format("data source='{0}'; initial catalog='{1}'; user id='{2}'; password='{3}'; Max Pool Size=1024; Pooling=true;Connection Timeout=180",
                new object[] { SqlServer, edmsDatabase, SqlUsername, sqlPassword});
        }

        public void Connect()
        {
            try
            {
                if (this.conn == null)
                {
                    this.conn = new SqlConnection(SQLConnectionHandler.connectionString);
                }
            }
            catch (Exception exception)
            {
                Exception ex = exception;
                throw new Exception(ex.Message);
            }
        }

        public void Delete(string sqlDelete)
        {
        }

        public string ExecuteScalar(string sqlSelect)
        {
            string str;
            try
            {
                SqlConnection sqlConnection = this.OpenConnection();
                SqlConnection sqlConnection1 = sqlConnection;
                this.conn = sqlConnection;
                using (sqlConnection1)
                {
                    str = (new SqlCommand(sqlSelect, this.conn)).ExecuteScalar().ToString();
                }
            }
            catch(NullReferenceException nullEx)
            {
                throw new DataException("SQL Select Error: No data returned for current select!", nullEx);
            }
            catch (Exception exception)
            {
                throw new Exception(exception.Message);
            }
            return str;
        }

        internal string GetConnectionString()
        {
            return SQLConnectionHandler.connectionString;
        }

        public DataTable GetDataTable(string sqlSelect)
        {
            DataTable dataTable;
            try
            {
                SqlConnection sqlConnection = this.OpenConnection();
                SqlConnection sqlConnection1 = sqlConnection;
                
                this.conn = sqlConnection;
                using (sqlConnection1)
                {
                    SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(new SqlCommand(sqlSelect, this.conn));
                    SqlCommandBuilder sqlCommandBuilder = new SqlCommandBuilder(sqlDataAdapter);
                    DataTable dt = new DataTable();
                    sqlDataAdapter.Fill(dt);
                    dataTable = dt;
                }
            }
            catch (Exception exception)
            {
                Exception ex = exception;
                throw new Exception(ex.Message);
            }
            return dataTable;
        }

        public DataTable GetDataTable(string tableName, string sqlWhere, int batchLimit)
        {
            DataTable dataTable;
            try
            {
                //string sqlSelect = string.Format("SELECT TOP {0} * FROM XTD_{1} WHERE {2}", batchLimit, tableName, sqlWhere);

                string sqlSelect = string.IsNullOrEmpty(sqlWhere) ? string.Format("SELECT TOP {0} * FROM {1}", batchLimit, tableName) 
                    : sqlSelect = string.Format("SELECT TOP {0} * FROM {1} WHERE {2}", batchLimit, tableName, sqlWhere);

                SqlConnection sqlConnection = this.OpenConnection();
                SqlConnection sqlConnection1 = sqlConnection;
                this.conn = sqlConnection;
                using (sqlConnection1)
                {
                    SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(new SqlCommand(sqlSelect, this.conn));
                    SqlCommandBuilder sqlCommandBuilder = new SqlCommandBuilder(sqlDataAdapter);
                    DataTable dt = new DataTable();
                    sqlDataAdapter.Fill(dt);
                    dataTable = dt;
                }
            }
            catch (Exception exception)
            {
                Exception ex = exception;
                throw new Exception(ex.Message);
            }
            return dataTable;
        }

        public SqlConnection OpenConnection()
        {
            SqlConnection sqlConnection;
            try
            {
                if (this.conn == null || this.conn.State == ConnectionState.Closed)
                {
                    this.conn = new SqlConnection(SQLConnectionHandler.connectionString);
                }
                this.conn.Open();
                sqlConnection = this.conn;
            }
            catch (Exception exception)
            {
                sqlConnection = null;
            }
            return sqlConnection;
        }

        public void Update(DataTable dt, string tableName)
        {
            try
            {
                SqlConnection sqlConnection = this.OpenConnection();
                SqlConnection sqlConnection1 = sqlConnection;
                this.conn = sqlConnection;
                using (sqlConnection1)
                {
                    SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(new SqlCommand(string.Format("select * from {0}", tableName), this.conn));
                    SqlCommandBuilder sqlCommandBuilder = new SqlCommandBuilder(sqlDataAdapter);
                    sqlDataAdapter.Update(dt);
                    dt.AcceptChanges();
                }
            }
            catch (Exception exception)
            {
                Exception ex = exception;
                throw new Exception(ex.Message);
            }
        }

        public void UpdateRow(DataRow dr)
        {
            SqlConnection sqlConnection = this.OpenConnection();
            SqlConnection sqlConnection1 = sqlConnection;
            this.conn = sqlConnection;
            using (sqlConnection1)
            {
                SqlCommandBuilder sqlCommandBuilder = new SqlCommandBuilder(new SqlDataAdapter(new SqlCommand("", this.conn)));
                dr.AcceptChanges();
            }
        }

        public void ExecuteSqlString(string sqlString)
        {
            try
            {
                SqlConnection sqlConnection = this.OpenConnection();
                
                using (sqlConnection)
                {
                    (new SqlCommand(sqlString, sqlConnection)).ExecuteNonQuery().ToString();
                }
            }
            catch (Exception exception)
            {
                throw new Exception(exception.Message);
            }
        }
    }
}
